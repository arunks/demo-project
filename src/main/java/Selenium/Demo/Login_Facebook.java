package Selenium.Demo;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Login_Facebook extends Page {

	Login_Facebook(WebDriver driver) {
		super(driver);
	}

	public static void main() throws IOException {

	}

	@FindBy(how = How.ID, using = "email")
	private WebElement emailTextField;

	@FindBy(how = How.ID, using = "pass")
	private WebElement passwordTextField;

	@FindBy(how = How.CSS, using = "input[value='Log In']")
	private WebElement loginButton;

	public Page login(String email, String password) {
		emailTextField.click();
		emailTextField.sendKeys(email);
		passwordTextField.click();
		passwordTextField.sendKeys(password);
		loginButton.click();
		return new userpage(driver);
	}

}
