package Selenium.Demo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Properties;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class TestBase {

	WebDriver driver;
	Properties prop;

	@BeforeTest
	public void load() throws IOException {
		// System.setProperty("webdriver.gecko.driver",
		// "/Users/nichesoft18/Documents/geckodriver");
		System.setProperty("webdriver.chrome.driver", "/Users/nichesoft18/Documents/chromedriver");
		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> prefs = new HashMap<String, Object>();
		prefs.put("profile.default_content_setting_values.notifications", 2);
		options.setExperimentalOption("prefs", prefs);
		// DesiredCapabilities caps = DesiredCapabilities.chrome();
		// caps.setBrowserName("chrome");
		// caps.setPlatform(Platform.MAC);
		// driver = new RemoteWebDriver(new URL("http://localhost:4445/wd/hub"),
		// options);
		// driver = new FirefoxDriver();
		driver = new ChromeDriver(options);
		driver.get("https://www.facebook.com");
		driver.manage().window().maximize();
		System.out.println(System.getProperty("user.dir") + "/credential.properties");
		InputStream input = new FileInputStream(System.getProperty("user.dir") + "/credential.properties");
		prop = new Properties();
		prop.load(input);
	}

	@AfterTest
	public void onfinish() {
		driver.quit();
	}

}
