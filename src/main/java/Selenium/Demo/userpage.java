package Selenium.Demo;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class userpage extends Page {

	userpage(WebDriver driver) {
		super(driver);
	}

	WebDriverWait wait = new WebDriverWait(driver, 20);

	@FindBy(how = How.XPATH, using = "//div[@id='u_w_2']//input[@placeholder='Search']")
	private WebElement searchBox;

	@FindBy(how = How.XPATH, using = "//input[@id='js_4p']")
	private WebElement chatsearch;

	@FindBy(how = How.XPATH, using = "//div[contains(text(),'Messenger')]")
	private WebElement messengerbutton;

	@FindBy(how = How.XPATH, using = "//input[@placeholder='Search Messenger']")
	private WebElement searchmessengertextbox;

	@FindBy(how = How.XPATH, using = "//div[@class='_1mf _1mj']")
	private WebElement textbox;

	@FindBy(how = How.CSS, using = "a[class='_30yy _38lh _7kpi']")
	private WebElement sendButton;

	@FindBy(how = How.XPATH, using = "//div[@class='_4rv4']/a")
	private WebElement postbutton;

	@FindBy(how = How.XPATH, using = "//a[@title='Go to Facebook Home']")
	private WebElement facebookbutton;

	@FindBy(how = How.XPATH, using = "//div[@id='userNavigationLabel']")
	private WebElement dropdown;

	@FindBy(how = How.XPATH, using = "//h2[@id='js_5']")
	private WebElement user;

	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Log Out')]")
	private WebElement logoutbutton;

	public void enterInSearchBox() throws InterruptedException {
		Actions action = new Actions(driver);
		String value = "Subham Joshi";
		char[] letter = value.toCharArray();
		messengerbutton.click();
		wait.until(ExpectedConditions.visibilityOf(searchmessengertextbox));
		searchmessengertextbox.click();
		for (int i = 0; i <= letter.length - 1; i++) {
			String j = new StringBuilder().append(letter[i]).toString();
			searchmessengertextbox.sendKeys(j);
		}
		Thread.sleep(2000);
		searchmessengertextbox.sendKeys(Keys.RETURN);
		Thread.sleep(2000);
		if (user.getText().equalsIgnoreCase(value))
			action.moveToElement(textbox).click().build().perform();
		action.moveToElement(textbox).sendKeys("subham is a idiot").build().perform();
		sendButton.click();
		Thread.sleep(2000);
		facebookbutton.click();
		wait.until(ExpectedConditions.visibilityOf(dropdown));
		dropdown.click();
		wait.until(ExpectedConditions.visibilityOf(logoutbutton));
		logoutbutton.click();
	}

}
